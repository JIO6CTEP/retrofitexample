package com.sdimdev.retrofitexample.api;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.sdimdev.retrofitexample.api.api.RetrofitException;
import com.sdimdev.retrofitexample.api.utils.ErrorUtils;

/**
 * Created by sdim on 16.11.2016.
 */

public abstract class BasicActivity extends AppCompatActivity implements IMessage {
    @Override
    public void message(int resString) {
        Toast.makeText(this, resString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String string) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
    }

    protected void defaultError(RetrofitException exception) {
        ErrorUtils.defaultError(exception, this);
    }
}
