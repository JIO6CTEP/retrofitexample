package com.sdimdev.retrofitexample.api.api;

import android.util.Log;

import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by sdim on 08.11.2016.
 */
public class RetrofitException extends RuntimeException {
    private final Response response;
    private final Kind kind;
    private final HttpErrorBody httpErrorBody;

    private RetrofitException(Response response, Kind kind, Throwable exception) {
        super(exception);
        this.response = response;
        this.kind = kind;
        httpErrorBody = kind == Kind.HTTP ? parseApiError(response, API.getRetrofit()) : null;
    }

    public static RetrofitException httpError(Response response) {
        String message = response.code() + " " + response.message();
        return new RetrofitException(response, Kind.HTTP, null);
    }

    public static RetrofitException networkError(IOException exception) {
        return new RetrofitException(null, Kind.NETWORK, exception);
    }

    public static RetrofitException unexpectedError(Throwable exception) {
        return new RetrofitException(null, Kind.UNEXPECTED, exception);
    }

    /**
     * HTTP response body converted to specified {@code type}. {@code null} if there is no
     * response.
     */
    private static HttpErrorBody parseApiError(Response<?> response, Retrofit retrofit) {
        final Converter<ResponseBody, HttpErrorBody> converter =
                retrofit
                        .responseBodyConverter(HttpErrorBody.class, new Annotation[0]);
        try {
            return converter.convert(response.errorBody());
        } catch (JsonSyntaxException e) {
            Log.d("RetrofitException", "convert HttpErrorBody error", e);
            return null;
        } catch (IOException e) {
            Log.d("RetrofitException", "convert HttpErrorBody error", e);
            return null;
        }

    }

    public HttpErrorBody getHttpErrorBody() {
        return httpErrorBody;
    }

    public Response getResponse() {
        return response;
    }

    public Kind getKind() {
        return kind;
    }

    public enum Kind {
        /**
         * An {@link IOException} occurred while communicating to the server.
         */
        NETWORK,
        /**
         * A non-200 HTTP status code was received from the server.
         */
        HTTP,
        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED
    }

}