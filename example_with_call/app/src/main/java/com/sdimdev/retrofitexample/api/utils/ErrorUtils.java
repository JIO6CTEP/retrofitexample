package com.sdimdev.retrofitexample.api.utils;

import com.sdimdev.retrofitexample.R;
import com.sdimdev.retrofitexample.api.IMessage;
import com.sdimdev.retrofitexample.api.api.RetrofitException;

/**
 * Created by sdim on 16.11.2016.
 */

public class ErrorUtils {
    public static void defaultError(RetrofitException retrofitException, IMessage messageShow) {
        switch (retrofitException.getKind()) {
            case NETWORK:
                messageShow.message(R.string.error_bad_server_connection);
                break;
            case HTTP:
                if (retrofitException.getHttpErrorBody() != null) {
                    messageShow.message(retrofitException.getHttpErrorBody().getMessage());
                    break;
                }
            case UNEXPECTED:
            default:
                messageShow.message(R.string.error_unknown);
                break;
        }
    }
}
