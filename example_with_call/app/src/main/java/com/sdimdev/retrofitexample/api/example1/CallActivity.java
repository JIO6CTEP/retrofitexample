package com.sdimdev.retrofitexample.api.example1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.sdimdev.retrofitexample.R;
import com.sdimdev.retrofitexample.api.BasicActivity;
import com.sdimdev.retrofitexample.api.api.API;
import com.sdimdev.retrofitexample.api.api.RetrofitException;
import com.sdimdev.retrofitexample.api.api.example1.ResponseExec;
import com.sdimdev.retrofitexample.api.model.Rate;

public class CallActivity extends BasicActivity {

    private final static int RUSSIA_ID = 298;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        TextView test = (TextView) findViewById(R.id.text);
        API.get().getRates(RUSSIA_ID).enqueue(new ResponseExec<Rate>() {
            @Override
            public void bodyResult(@NonNull Rate body) {
                test.setText(String.format("1BYN = %.2f %s", body.getCur_OfficialRate(), body.getCur_Name()));
            }

            @Override
            public void error(RetrofitException retrofitException) {
                super.error(retrofitException);
                defaultError(retrofitException);
            }
        });
    }
}
