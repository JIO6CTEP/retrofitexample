package com.sdimdev.retrofitexample.api;

/**
 * Created by sdim on 16.11.2016.
 */

public interface IMessage {
    void message(int resString);

    void message(String string);
}
