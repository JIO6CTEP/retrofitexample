package com.sdimdev.retrofitexample.api.model;

/**
 * Created by sdim on 08.11.2016.
 */
public class Rate {
    long Cur_ID;
    String Date;
    String Cur_Abbreviation;
    String Cur_Name;
    double Cur_Scale;
    double Cur_OfficialRate;

    public long getCur_ID() {
        return Cur_ID;
    }

    public void setCur_ID(long cur_ID) {
        Cur_ID = cur_ID;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getCur_Abbreviation() {
        return Cur_Abbreviation;
    }

    public void setCur_Abbreviation(String cur_Abbreviation) {
        Cur_Abbreviation = cur_Abbreviation;
    }

    public String getCur_Name() {
        return Cur_Name;
    }

    public void setCur_Name(String cur_Name) {
        Cur_Name = cur_Name;
    }

    public double getCur_Scale() {
        return Cur_Scale;
    }

    public void setCur_Scale(double cur_Scale) {
        Cur_Scale = cur_Scale;
    }

    public double getCur_OfficialRate() {
        return Cur_OfficialRate;
    }

    public void setCur_OfficialRate(double cur_OfficialRate) {
        Cur_OfficialRate = cur_OfficialRate;
    }
}
