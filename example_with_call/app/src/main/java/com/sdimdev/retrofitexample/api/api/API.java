package com.sdimdev.retrofitexample.api.api;


import com.sdimdev.retrofitexample.api.api.gson.LenientGsonConverterFactory;
import com.sdimdev.retrofitexample.api.utils.MLog;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Retrofit;

/**
 * Created by dzmitry on 30.8.16.
 */
public class API {

    private final static String TAG = "API";
    private static ApiFunctions api;
    private static Retrofit retrofit;

    private API() {
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    private static String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            RequestBody body = copy.body();
            if (body != null) {
                body.writeTo
                        (buffer);
                return buffer.readUtf8();
            } else {
                return "get method:" +
                        request.url().encodedQuery();
            }
        } catch (final IOException e) {
            return "did not work";
        }
    }

    private static OkHttpClient createClient(String url, OkHttpClient.Builder httpClient) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpClient.build();
    }

    private static Interceptor createInterceptor(String url) {
        return chain -> {
            Request original = chain.request();
            Request.Builder builder = original.newBuilder()
                    .method(original.method(), original.body());
            //   .build();
            MLog.d("API", "url:" + original.url() + "; body:" + bodyToString(original));
            Request request = builder.build();
            Response r = chain.proceed(request);

            String bodyString = r.body().string();
            MLog.d("API", "response: " + bodyString);
            return r.newBuilder()
                    .body(ResponseBody.create(r.body().contentType(), bodyString))
                    .build();
        };
    }


    private static Retrofit createRetrofit(String url) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(createInterceptor(url));
        httpClient.connectTimeout(20, TimeUnit.SECONDS);
        httpClient.readTimeout(20, TimeUnit.SECONDS);
        httpClient.writeTimeout(20, TimeUnit.SECONDS);
        OkHttpClient client = createClient(url, httpClient);

        Retrofit retrofit;

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(LenientGsonConverterFactory.create())
                .client(client);
        retrofit = builder.build();
        return retrofit;
    }

    public static ApiFunctions get() {
        if (api == null) {
            createApi(ApiConst.url);
        }
        return api;
    }

    private static void createApi(String url) {
        retrofit = createRetrofit(url);
        api = retrofit.create(ApiFunctions.class);
    }
}
