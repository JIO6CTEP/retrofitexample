package com.sdimdev.retrofitexample.api.api;


import com.sdimdev.retrofitexample.api.model.Rate;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


/**
 * Created by dzmitry on 30.8.16.
 */
public interface ApiFunctions {

    @GET("Rates/{country_id}")
    Call<Rate> getRates(@Path("country_id") long country_id);
}
