package com.sdimdev.retrofitexample.api.api.example_lamda;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.sdimdev.retrofitexample.api.api.RetrofitException;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sdim on 16.11.2016.
 */

public class LamdaResponseExec<T> implements Callback<T> {
    private final static String TAG = "LamdaResponseExec";

    private Action<T> bodyAction = null;
    private Action<RetrofitException> exceptionAction = null;

    public LamdaResponseExec(@NonNull Action<T> action) {
        bodyAction = action;
    }

    public LamdaResponseExec(@NonNull Action<T> action, @Nullable Action<RetrofitException> exceptionAction) {
        this.bodyAction = action;
        this.exceptionAction = exceptionAction;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            T body = response.body();
            bodyResult(body);
        } else {
            error(RetrofitException.httpError(response));
        }
    }

    private void error(RetrofitException retrofitException) {
        if (exceptionAction != null)
            exceptionAction.call(retrofitException);
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (t instanceof IOException)
            error(RetrofitException.networkError((IOException) t));
        else {
            error(RetrofitException.unexpectedError(t));
            Log.d(TAG, "error RetrofitException", t);
        }
    }

    private void bodyResult(@NonNull T body) {
        bodyAction.call(body);
    }

    public interface Action<R> {
        void call(R object);
    }
}