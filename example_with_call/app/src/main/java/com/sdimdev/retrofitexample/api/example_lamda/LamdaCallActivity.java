package com.sdimdev.retrofitexample.api.example_lamda;

import android.os.Bundle;
import android.widget.TextView;

import com.sdimdev.retrofitexample.R;
import com.sdimdev.retrofitexample.api.BasicActivity;
import com.sdimdev.retrofitexample.api.api.API;
import com.sdimdev.retrofitexample.api.api.example_lamda.LamdaResponseExec;
import com.sdimdev.retrofitexample.api.model.Rate;

public class LamdaCallActivity extends BasicActivity {

    private final static int RUSSIA_ID = 298;
    TextView test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        test = (TextView) findViewById(R.id.text);
       /* API.get().getRates(RUSSIA_ID).enqueue(new LamdaResponseExec<Rate>(
                object ->  {test.setText(String.format("%.2f %s", object.getCur_OfficialRate(), object.getCur_Name()))
                //more
                }
                ,object ->defaultError(object)));*/

        API.get().getRates(RUSSIA_ID).enqueue(new LamdaResponseExec<Rate>(this::setData, this::defaultError));
    }

    void setData(Rate r) {
        test.setText(String.format("1BYN = %.2f %s", r.getCur_OfficialRate(), r.getCur_Name()));
    }
}
