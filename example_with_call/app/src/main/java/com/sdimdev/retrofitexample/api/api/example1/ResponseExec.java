package com.sdimdev.retrofitexample.api.api.example1;

import android.support.annotation.NonNull;
import android.util.Log;

import com.sdimdev.retrofitexample.api.api.RetrofitException;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sdim on 16.11.2016.
 */

public abstract class ResponseExec<T> implements Callback<T> {
    private final static String TAG = "ResponseExec";

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            T body = response.body();
            bodyResult(body);
        } else {
            error(RetrofitException.httpError(response));
        }
    }

    public void error(RetrofitException retrofitException) {
        /* default empty */
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (t instanceof IOException)
            error(RetrofitException.networkError((IOException) t));
        else {
            error(RetrofitException.unexpectedError(t));
            Log.d(TAG, "error RetrofitException", t);
        }
    }

    public abstract void bodyResult(@NonNull T body);
}