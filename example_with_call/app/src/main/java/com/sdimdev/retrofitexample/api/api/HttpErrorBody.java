package com.sdimdev.retrofitexample.api.api;

/**
 * Created by sdim on 08.11.2016.
 */

/**
 * You HttpError body class
 */
public class HttpErrorBody {

    private int error;
    private String message;

    public HttpErrorBody(int error) {
        this.error = error;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
